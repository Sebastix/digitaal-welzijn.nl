/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {},
  },
  // add daisyUI plugin
  plugins: [
    require("@tailwindcss/typography"), // https://tailwindcss.com/docs/typography-plugin & https://daisyui.com/docs/layout-and-typography/#-1
    require('daisyui')
  ],
  // daisyUI config - https://daisyui.com/docs/config/
  daisyui: {
    styled: true,
    themes: ['fantasy'],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: "",
    darkTheme: "fantasy",
  },
}
