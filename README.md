# digitaal-welzijn.nl

@todo write introduction of the purpose of this project

## How to contribute

@todo

## Team
* Sebastian Hagens @sebastix - initiator, maintainer

## License

digitaal-welzijn.nl of Sebastix is licensed under a
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)  
![](https://i.creativecommons.org/l/by/4.0/88x31.png) 

---
### Technical stack

* Nuxt v3 - https://v3.nuxtjs.org/
* Nuxt Content - https://content.nuxtjs.org/
* TailwindCSS - https://tailwindcss.com/
  * Typography plugin - https://tailwindcss.com/docs/typography-plugin
* DaisyUI - https://daisyui.com/
  * using the fantasy theme 

### Setup for development

Make sure you have installed:
* Node.js (latest LTS version)

Install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

### Run your local development server

Start the development server on http://localhost:3000

```bash
npm run dev
```

### Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Build static website

```bash
npm run generate
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.
