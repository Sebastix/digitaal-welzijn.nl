---
title: 'Presentaties'
description: 'meta description of the page'
draft: false
navigation: true
layout: default
---

## Do your own research
# Presentaties

* [Data is the new gold, who are the new thieves? | Tijmen Schep | TEDxUtrecht
  ](https://youtu.be/XNF-rGiGb50)
* [The Power of Big Data and Psychographics | 2016 Concordia Annual Summit
  ](https://www.youtube.com/watch?v=n8Dd5aVXLCc)
* [Privacy is progress | Daphne Muller | TEDxVenlo](https://www.youtube.com/watch?v=H5fVIYCt_w8)