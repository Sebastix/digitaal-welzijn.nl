---
title: 'Digitaal welzijn'
description: 'Verbeter je digitaal welzijn door minder BigTech en social media te gebruiken.'
draft: false
navigation: false
layout: default
---

Verbeter je digitaal welzijn door minder BigTech en social media te gebruiken.

[Begin nu met deze checklist](1.checklist-minder-bigtech.md)