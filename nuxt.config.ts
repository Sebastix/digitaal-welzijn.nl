import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@nuxt/content',
    ['@nuxtjs/tailwindcss']
  ],
  // css: [
  //   '@/assets/scss/site.scss'
  // ],
  // Workaround for the following above - see https://github.com/nuxt/framework/issues/4269#issuecomment-1095557068
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/scss/site.scss";',
        },
      },
    },
  },
  content: {
    // https://content.nuxtjs.org/api/configuration
  }
})
